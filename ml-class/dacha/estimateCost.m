[X, y] = readData('dacha.csv');

modelfun = @(b,x)b(1) + b(2)*x(:,1) + b(3)*x(:,2) + ...
    b(4)*x(:,3) + b(5)*x(:,4) + b(6)*x(:,5) + b(7)*x(:,6) + ...
    b(8)*x(:,7) + b(9)*x(:,1).^2 + b(10)*x(:,2).^2 + ...
    b(11)*x(:,4).^2 + b(12)*x(:,1).*x(:,2) + ...
    b(13)*x(:,1).*x(:,4) + b(14)*x(:,2).*x(:,4);
beta0 = zeros(1,14);
mdl = fitnlm(X,y,modelfun,beta0);

testHouse = [45 6.0 0 40 1 0 0];
myHouse = [35 5.5 1 50 1 0 0];
fprintf('Check(1150): %f\n', predict(mdl, testHouse));
fprintf('Predicted cost: %f\n', predict(mdl, myHouse));