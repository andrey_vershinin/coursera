function [X, y] = readData(filename)
    delimiter = ',';
    startRow = 2;
    % Format string for each line of text:
    %   Cost: double (%f)
    %	From MKAD: double (%f)
    %   Area: double (%f)
    %	isBrick: double (%f)
    %   House area: double (%f)
    %	hasWater: double (%f)
    %   hasWC: double (%f)
    %	hasGas: double (%f)
    formatSpec = '%f%f%f%f%f%f%f%f%[^\n\r]';
    fileID = fopen(filename,'r');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
    fclose(fileID);
    data = [dataArray{1:end-1}];
    y = data(:,1);
    X = data(:,2:end);
end