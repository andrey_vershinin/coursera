function a = e(t, e) 
    a = (2 * (t + e)^3 - 2 * (t - e) ^ 3 ) / (2 * e);
end