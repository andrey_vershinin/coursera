 sigma_e=0.018; 
 phi=0.94; 
 sigma_d=0.18;
 rho=-0.80756;
 m = [sigma_e 0; rho * sigma_d sigma_d * sqrt(1-rho^2) ];
 
 %zw = mvnrnd(0,[1 0; 0 1]);

 mu = [0 0];
Sigma = [.25 .3; .3 1];
x1 = -3:.2:3; x2 = -3:.2:3;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
surf(x1,x2,F);
caxis([min(F(:))-.5*range(F(:)),max(F(:))]);
axis([-3 3 -3 3 0 .4])
xlabel('x1'); ylabel('x2'); zlabel('Probability Density');