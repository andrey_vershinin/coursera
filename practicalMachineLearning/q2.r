#1
library(AppliedPredictiveModeling)
library(caret)
data(AlzheimerDisease)
adData = data.frame(diagnosis,predictors)
trainIndex = createDataPartition(diagnosis,p=0.5,list=FALSE)
training = adData[trainIndex,]
testing = adData[trainIndex,]

adData = data.frame(diagnosis,predictors)
trainIndex = createDataPartition(diagnosis, p = 0.50,list=FALSE)
training = adData[trainIndex,]
testing = adData[-trainIndex,]



#2
library(AppliedPredictiveModeling)
data(concrete)
library(caret)
set.seed(1000)
inTrain = createDataPartition(mixtures$CompressiveStrength, p = 3/4)[[1]]
training = mixtures[ inTrain,]
testing = mixtures[-inTrain,]

qplot(training$Superplasticizer)


#3
library(caret)
library(AppliedPredictiveModeling)
set.seed(3433)
data(AlzheimerDisease)
adData = data.frame(diagnosis,predictors)
inTrain = createDataPartition(adData$diagnosis, p = 3/4)[[1]]
training = adData[ inTrain,]
testing = adData[-inTrain,]

preProcess(training[,grepl("^IL", names(training))], method = "pca", thresh = 0.9)


#4
library(caret)
library(AppliedPredictiveModeling)
set.seed(3433)
data(AlzheimerDisease)
adData = data.frame(diagnosis,predictors)
inTrain = createDataPartition(adData$diagnosis, p = 3/4)[[1]]
training = adData[ inTrain,]
testing = adData[-inTrain,]

t_new <- training[,grepl("^IL|diagnosis", names(training))]
test_new <- testing[,grepl("^IL|diagnosis", names(training))]

pcaPP <- preProcess(t_new[,-1], method = "pca", thresh = 0.8)
trainPca <- predict(pcaPP, t_new[,-1])
m_pca <- train(t_new$diagnosis~.,method="glm",data=trainPca)

testPca <- predict(pcaPP, test_new[,-1])
confusionMatrix(test_new$diagnosis, predict(m_pca, testPca))

nonpca <- train(diagnosis~., data = t_new, method="glm")
confusionMatrix(test_new$diagnosis, predict(nonpca, test_new))

